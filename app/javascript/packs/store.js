import axios from 'axios'

const state = {
  todo_input: '',
  todos: []
}

const mutations = {
  setTodos(state, todos){
      state.todos = todos
  },
  setTodoInput(state, todoInput){
      state.todo_input = todoInput
  },
  removeTodo(state, id) {
    const index = state.todos.findIndex((todo) => {
      return todo.id === id
    })
    state.todos.splice(index, 1)
  }
}

const actions = {
  fetchTodos(context) {
    axios.get('./api/v1/todos')
      .then(res => {
        context.commit('setTodos', res.data)
      })
      .catch(error => {
        throw error
      })
  },
  postTodo(context, todo) {
    axios.post('/api/v1/todos', todo)
      .then(res => {
        context.commit('setTodoInput', '')
        context.dispatch('fetchTodos')
      })
      .catch(error => {
        throw error
      })
  },
  complete(context, todo) {
    const todoUpdate = { 
      "todo": 
      { "completed": !todo.completed } 
    }
    axios.put('/api/v1/todos/' + todo.id, todoUpdate)
      .then(res => {
        context.dispatch('fetchTodos')
      })
      .catch(error => {
        throw error
      })
  },
  deleteTodo(context, todoItem) {
    axios.delete('/api/v1/todos/' + todoItem.id)
      .then(res => {
        context.commit('removeTodo', todoItem.id)
      })
      .catch(error => {
        throw error
      })
  }
}

export default {
  state,
  mutations,
  actions
}